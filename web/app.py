"""
Author: Scott Choi, schoi@uoregon.edu
Functionality: app.py designed to introduce me to using Flask and Docker.
"""
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.errorhandler(403)
def file_forbidden(error):
    return render_template("403.html"), 403

@app.errorhandler(404)
def page_not_found(error):
    return render_template("404.html"), 404

#@app.route('/<name>') #Not good enough
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def existing_file(path):
    path = request.environ['RAW_URI']
    if '~' in path or '..' in path or '//' in path: # 403 Error 
        return file_forbidden(path)
        #return render_template("403.html")
    try:
        return render_template(path) # If render_template succeeds there exists a matching file in template folder
    except:
        #return render_template("404.html")
        return page_not_found(path)

if __name__ == "__main__":
    app.run(port= 5000, debug=True, host='0.0.0.0')
